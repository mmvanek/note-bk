# CS498 STK Final Project
# Note.bk

# Adam Blackburn :: blackbu3
# Kashev Dalmia  :: dalmia3
# Matt Vanek     :: vanek1

# main.py

# This file contains all the handler classes for URI routing.

import string
import random
import logging
import os

from google.appengine.api import users
from google.appengine.ext.ndb import Key
from django.utils.simplejson import loads, dumps
from google.appengine.ext.webapp import template
from google.appengine.api import mail
from google.appengine.api import memcache

from models import NoteModel

import webapp2

def id_generator(size=6, chars=string.letters + string.digits):
    '''generates random IDs for notes, users'''
    return ''.join(random.choice(chars) for x in range(size))

def fail_invalid_id(r):
    r.error(404)
    r.response.write("invalid request")

def fail_invalid_user(r):
    r.error(401)
    r.response.write("unauthorized")

class RestfulHandler(webapp2.RequestHandler):
    ''' RESTful API; get, post, put, delete '''
    def get(self, id_str):
        self.error(501)
        self.response.write('Feature not implemented.')

    def post(self, id_str=None):
        self.error(501)
        self.response.write('Feature not implemented.')

    def put(self, id_str):
        self.error(501)
        self.response.write('Feature not implemented.')

    def delete(self, id_str):
        self.error(501)
        self.response.write('Feature not implemented.')


def restful_handler_factory(Model, authorization_required=True):
    class ModelHandler(RestfulHandler):
        ''' A generic REST handler for AppEngine '''

        def get(self, id_str=None):
            user = users.get_current_user()

            if authorization_required and not user:
                fail_invalid_user(self)
                return

            if id_str:
                # respond with a specific object
                obj = Model.get_by_id(id_str)

                if not obj:
                    fail_invalid_id(self)
                    return

                if obj.owner == user.user_id():
                    self.response.headers['Content-Type'] = 'application/json'
                    self.response.write(dumps(obj.safe_to_dict_with_id()));
                else:
                    fail_invalid_user(self)
                    return
            else:
                # respond with all the objects for this user
                all_query = Model.query(Model._properties['owner'] == user.user_id())
                # need to filter this by owner somehow
                objs = []
                for obj in all_query:
                    objs.append(obj.safe_to_dict_with_id())
                self.response.headers['Content-Type'] = 'application/json'
                self.response.write(dumps(objs))

        def __populate_from_json(self, obj, dict_from_json):
            if 'id' in dict_from_json:
                del dict_from_json['id']
            obj.populate(**dict_from_json)
            
        def post(self, id_str=None):
            user = users.get_current_user()

            if authorization_required and not user:
                fail_invalid_user(self)
                return

            # create a new model
            obj = Model(id=id_generator(16))
            obj_from_user = loads(self.request.body)
            self.__populate_from_json(obj, obj_from_user)
            obj.owner = user.user_id();
            obj.put()

            memcache.incr('note-count')
            #self.response.headers['Content-Type'] = 'application/json'
            #self.response.write(dumps(obj.safe_to_dict_with_id()));

        def put(self, id_str):
            user = users.get_current_user()

            if authorization_required and not user:
                fail_invalid_user(self)
                return

            obj = Model.get_by_id(id_str)

            if not obj:
                fail_invalid_id(self)
                return

            obj_from_user = loads(self.request.body)
            self.__populate_from_json(obj, obj_from_user)

            if obj.owner == user.user_id():
                obj.put()
            else:
                fail_invalid_user(self)
                return
            
            # self.response.headers['Content-Type'] = 'application/json'
            # self.response.write(dumps(obj.safe_to_dict_with_id()));


        def delete(self, id_str):
            user = users.get_current_user()

            if authorization_required and not user:
                fail_invalid_user(self)
                return

            obj = Model.get_by_id(id_str)

            if not obj:
                fail_invalid_id(self)
                return

            if obj.owner == user.user_id():
                obj.key.delete()
                memcache.decr('note-count')
            else:
                fail_invalid_user(self)
                return

    return ModelHandler

class MailHandler(webapp2.RequestHandler):
    '''a class that simply checks if the user is logged in or not'''
    def post(self, id_str):
        user = users.get_current_user()

        if not user:
            fail_invalid_user(self)
            return

        obj = NoteModel.get_by_id(id_str)

        if not obj:
            fail_invalid_id(self)
            return

        to_addr = self.request.body
        if not mail.is_email_valid(to_addr):
            self.error(500)
            self.response.write('invalid address')
            return

        message = mail.EmailMessage()
        message.sender = user.email()
        message.to = to_addr
        message.subject = '[note-bk] %s' % obj.name
        message.body = obj.contents
        message.send()

        self.response.write("ok")

class UserChecker(webapp2.RequestHandler):
    '''a class that simply checks if the user is logged in or not'''
    def get(self):
        user = users.get_current_user()
        if user is not None:
            self.response.write('true');
        else:
            self.response.write('false');

class NoteCounter(webapp2.RequestHandler):
    '''a class that simply checks if the user is logged in or not'''
    def get(self):
        data = memcache.get('note-count')
        if data:
            logging.info('memcache success')
            self.response.write(data)
        else:
            # we don't really care if someone submit/deletes a note
            # while we're counting
            count = NoteModel.query().count(10000)
            self.response.write(count)
            memcache.set('note-count', count)

class LoginLogoutURLGetter(webapp2.RequestHandler):
    '''a class that returns the login or logout urls for app engine'''
    def get(self, inout):
        if inout == 'ins':
            self.response.write(users.create_login_url('/'))
        else: # inout == 'out':
            self.response.write(users.create_logout_url('/'))



app = webapp2.WSGIApplication([# ('/', MainHandler), the / handler is set in app.yaml
                               ('/note(?:/([A-Za-z0-9]{16}))?', restful_handler_factory(NoteModel)),
                               ('/note/([A-Za-z0-9]{16})/mail', MailHandler),
                               #('/user(?:/([A-Za-z0-9]{16}))?', restful_handler_factory(UserModel, False)),
                               #('/all/(\d+)', AllHandler),
                               ('/num', NoteCounter),
                               ('/usercheck', UserChecker),
                               ('/userurl/(ins|out)', LoginLogoutURLGetter)
                              ], debug=True)
