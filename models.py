# CS498 STK Final Project
# Note.bk

# Adam Blackburn :: blackbu3
# Kashev Dalmia  :: dalmia3
# Matt Vanek     :: vanek1

# models.py

# This file contains all the datastore models for storage in
# the ndb datastore and the memcache.

from google.appengine.ext import ndb       # ndb!
from google.appengine.api import memcache  # memcache!
from google.appengine.api import users     # users!

class BaseModel(ndb.Model):
    def safe_to_dict(self):
        return self.to_dict() #exclude=['owner'])

    def safe_to_dict_with_id(self):
        d = self.safe_to_dict()
        d['id'] = self.key.id()
        return d

class NoteModel(BaseModel):
    '''a note model in the datastore. it has an owner, text,
       and some other metadata'''
    # id := a random id.
    name     = ndb.StringProperty(indexed=False)
    owner    = ndb.StringProperty()
    kind     = ndb.IntegerProperty(indexed=False)
    order    = ndb.IntegerProperty(indexed=False)
    contents = ndb.TextProperty(indexed=False)
