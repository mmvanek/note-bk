import unittest
import webapp2

import main

from models import NoteModel
from google.appengine.ext import testbed
from google.appengine.api import users

from django.utils.simplejson import loads, dumps

class TestModelHandlerWithInvalidUser(unittest.TestCase):
	def setUp(self):
		self.testbed = testbed.Testbed()
		self.testbed.setup_env(USER_EMAIL='',USER_ID='', USER_IS_ADMIN='', overwrite=True)
		# Then activate the testbed, which prepares the service stubs for use.
		self.testbed.activate()
		self.testbed.init_user_stub()
		self.testbed.init_datastore_v3_stub()
		self.testbed.init_memcache_stub()
		handler = main.restful_handler_factory(NoteModel)
		NoteModel(id='ABCDEFGHIJKLMNOP', contents="note note note", name="note", owner="1", kind=1, order=1).put()
		NoteModel(id='DELETEMEIJKLMNOP', contents="note note note", name="note", owner="1", kind=1, order=1).put()
		self.fake_app = webapp2.WSGIApplication([('/note(?:/([A-Za-z0-9]{16}))?', handler)])

	def tearDown(self):
		self.testbed.deactivate()

	def testGetWithInvalidUser(self):
		request = webapp2.Request.blank('/note/ABCDEFGHIJKLMNOP')
		request.method = 'GET'
		response = request.get_response(self.fake_app)

		self.assertEqual(response.status_int, 401)

	def testPostWithInvalidUser(self):
		request = webapp2.Request.blank('/note/ABCDEFGHIJKLMNOP')
		request.method = 'POST'
		note = NoteModel(id='ABCDEFGHIJKLMNO1', contents="note note note", name="note", owner="1", kind=1, order=1)
		request.body = dumps(note.safe_to_dict_with_id())
		response = request.get_response(self.fake_app)

		# no body in resposne from server
		self.assertEqual(response.status_int, 401)

	def testPutWithInvalidUser(self):
		request = webapp2.Request.blank('/note/ABCDEFGHIJKLMNOP')
		request.method = 'PUT'
		note = NoteModel(id='ABCDEFGHIJKLMNOP', contents="note note note", name="note", owner="1", kind=1, order=1)
		request.body = dumps(note.safe_to_dict_with_id())
		response = request.get_response(self.fake_app)

		# no body in resposne from server
		self.assertEqual(response.status_int, 401)


	def testDeleteWithInvalidUser(self):
		request = webapp2.Request.blank('/note/DELETEMEIJKLMNOP')
		request.method = 'DELETE'
		response = request.get_response(self.fake_app)

		# no body in resposne from server
		self.assertEqual(response.status_int, 401)
