// CS498 STK Final Project
// Note.bk

// Adam Blackburn :: blackbu3
// Kashev Dalmia  :: dalmia3
// Matt Vanek     :: vanek1

// qunit-tests.js
// The tests that QUnit runs on the Note.bk Code Base


/* utils.js - General Tests */
module('utils.js - general', {
    setup : function(){
        RegisterPrecompiledTemplates();
    }
});
// Test added jQuery Exists Method
test('jQuery Exists Method', 2, function() {
    ok(($('#content').exists() === true),
       'Correctly finds IDs that do exist.');

    ok(($('#garbage-pants').exists() === false),
       'Correctly does not find IDs that do not exist.');
});

// Test Dust Functions
test('Dust Render Wrapper', 2, function() {
    // Register Precompiled Templates
    
    DustRender('splash', {}, '#content', function() {
        ok($('#login').exists(), 'login button is properly rendered into page.');
    });
    
    DustRender('notebk', {}, '#content', function() {
        ok($('#note-app').exists(), 'note-app is properly rendered into page.');
    });
});

// Logged in/out testing functions.
test('Logging In & Out Functions', 1, function(){
    // User must be logged in to see this page (as an admin) so...
    ok((IsLoggedIn()), 'User is successfully Detected as Logged In');
});

/* util.js - Epic Editor */
module('utils.js - epic', {
    setup : function(){
        RegisterPrecompiledTemplates();
        DustRender('notebk', {}, '#content');
        LoadEpicEditor();
    }
});

// Test Epic Editor Basic Functions
test('Epic Editor Basic Functions', 2, function() {
    ok((SetEpicSave != undefined), 'Epic Save Hook is defined.');

    // Ensure that Getting and Setting Editor Content Works.
    var test = 'A testing String'
    SetEpicContents(test);
    ok((GetEpicContents() === test), 'Getting and Setting Text Works.');
});

// Test Epic Editor autosaving.
test('Epic Editor Autosaving', 1, function() {
    stop();
    var count = 0;

    SetEpicSave(function(){
        count++
    });

    setTimeout(function(){
        ok(((count > 2) && (count < 20)),
            'Autosaving works at regular intervals; '+String(count)+' Times');
        UnsetEpicSave();
        start();
    }, 2000);
});
