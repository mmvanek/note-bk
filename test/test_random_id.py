import unittest
import webapp2

from main import id_generator

class RestfulHandlerAlwaysFailTestCase(unittest.TestCase):
	def setUp(self):
		pass

	def tearDown(self):
		pass

	def testRandomId(self):
		idx = id_generator(16)

		self.assertEquals(len(idx), 16)
