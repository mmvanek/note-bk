import unittest
import webapp2

import main

from models import NoteModel
from google.appengine.ext import testbed
from google.appengine.api import users

from django.utils.simplejson import loads, dumps

class FailFunctionsAlwaysFailTestCase(unittest.TestCase):
	def setUp(self):
		pass

	def tearDown(self):
		pass

	def testFailInvalidIdFails(self):
		request = webapp2.Request.blank('/')

		class TestApp(webapp2.RequestHandler):
			def get(self):
				main.fail_invalid_id(self)

		fake_app = webapp2.WSGIApplication([('/', TestApp)])
		response = request.get_response(fake_app)

		self.assertEqual(response.status_int, 404)

	def testFailInvalidUserFails(self):
		request = webapp2.Request.blank('/')

		class TestApp(webapp2.RequestHandler):
			def get(self):
				main.fail_invalid_user(self)

		fake_app = webapp2.WSGIApplication([('/', TestApp)])
		response = request.get_response(fake_app)

		self.assertEqual(response.status_int, 401)

class RestfulHandlerAlwaysFailTestCase(unittest.TestCase):
	def setUp(self):
		self.fake_app = webapp2.WSGIApplication([('/note(?:/([A-Za-z0-9]{16}))?', main.RestfulHandler)])

	def tearDown(self):
		pass

	def testGetFails(self):
		request = webapp2.Request.blank('/note/ABCDEFGHIJKLMNOP')
		request.method = 'GET'
		response = request.get_response(self.fake_app)

		self.assertEqual(response.status_int, 501)


	def testPostFails(self):
		request = webapp2.Request.blank('/note')
		request.method = 'POST'
		response = request.get_response(self.fake_app)

		self.assertEqual(response.status_int, 501)


	def testPutFails(self):
		request = webapp2.Request.blank('/note/ABCDEFGHIJKLMNOP')
		request.method = 'PUT'
		response = request.get_response(self.fake_app)

		self.assertEqual(response.status_int, 501)


	def testDeleteFails(self):
		request = webapp2.Request.blank('/note/ABCDEFGHIJKLMNOP')
		request.method = 'DELETE'
		response = request.get_response(self.fake_app)

		self.assertEqual(response.status_int, 501)

class TestModelHandlerWithValidUser(unittest.TestCase):
	def setUp(self):
		self.testbed = testbed.Testbed()
		# Then activate the testbed, which prepares the service stubs for use.
		self.testbed.setup_env(USER_EMAIL='usermail@gmail.com',USER_ID='1', USER_IS_ADMIN='0', overwrite=True)
		self.testbed.activate()
		self.testbed.init_user_stub()
		self.testbed.init_datastore_v3_stub()
		self.testbed.init_memcache_stub()
		handler = main.restful_handler_factory(NoteModel)
		NoteModel(id='ABCDEFGHIJKLMNOP', contents="note note note", name="note", owner="1", kind=1, order=1).put()
		NoteModel(id='DELETEMEIJKLMNOP', contents="note note note", name="note", owner="1", kind=1, order=1).put()
		self.fake_app = webapp2.WSGIApplication([('/note(?:/([A-Za-z0-9]{16}))?', handler)])


	def tearDown(self):
		self.testbed.deactivate()

	def testGetSingleWithValidUser(self):
		request = webapp2.Request.blank('/note/ABCDEFGHIJKLMNOP')
		request.method = 'GET'
		response = request.get_response(self.fake_app)

		ret_val = loads(response.body)

		self.assertEqual(response.status_int, 200)

		self.assertTrue('id' in ret_val)
		self.assertTrue('name' in ret_val)
		self.assertTrue('contents' in ret_val)
		self.assertTrue('owner' in ret_val)

		self.assertEquals(ret_val['id'], 'ABCDEFGHIJKLMNOP')
		self.assertEquals(ret_val['name'], 'note')
		self.assertEquals(ret_val['contents'], 'note note note')
		self.assertEquals(ret_val['owner'], '1')

	def testGetAllWithValidUser(self):
		request = webapp2.Request.blank('/note')
		request.method = 'GET'
		response = request.get_response(self.fake_app)

		ret_val = loads(response.body)[0]

		self.assertEqual(response.status_int, 200)

		self.assertTrue('id' in ret_val)
		self.assertTrue('name' in ret_val)
		self.assertTrue('contents' in ret_val)
		self.assertTrue('owner' in ret_val)

		self.assertEquals(ret_val['id'], 'ABCDEFGHIJKLMNOP')
		self.assertEquals(ret_val['name'], 'note')
		self.assertEquals(ret_val['contents'], 'note note note')
		self.assertEquals(ret_val['owner'], '1')

	def testPostWithValidUser(self):
		request = webapp2.Request.blank('/note/ABCDEFGHIJKLMNOP')
		request.method = 'POST'
		note = NoteModel(id='ABCDEFGHIJKLMNO1', contents="note note note", name="note", owner="1", kind=1, order=1)
		request.body = dumps(note.safe_to_dict_with_id())
		response = request.get_response(self.fake_app)

		# no body in resposne from server
		self.assertEqual(response.status_int, 200)

	def testPutWithValidUser(self):
		request = webapp2.Request.blank('/note/ABCDEFGHIJKLMNOP')
		request.method = 'PUT'
		note = NoteModel(id='ABCDEFGHIJKLMNOP', contents="note note note", name="note", owner="1", kind=1, order=1)
		request.body = dumps(note.safe_to_dict_with_id())
		response = request.get_response(self.fake_app)

		# no body in resposne from server
		self.assertEqual(response.status_int, 200)


	def testDeleteWithValidUser(self):
		request = webapp2.Request.blank('/note/DELETEMEIJKLMNOP')
		request.method = 'DELETE'
		response = request.get_response(self.fake_app)

		# no body in resposne from server
		self.assertEqual(response.status_int, 200)


