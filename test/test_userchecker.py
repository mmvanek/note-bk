import unittest
import webapp2

from main import UserChecker, LoginLogoutURLGetter

from models import NoteModel
from google.appengine.ext import testbed
from google.appengine.api import users

from django.utils.simplejson import loads, dumps

class TestUserCheckerHandler(unittest.TestCase):
	def setUp(self):
		pass

	def tearDown(self):
		pass

	def testVerifyUserLoggedIn(self):
		xtestbed = testbed.Testbed()
		# Then activate the xtestbed, which prepares the service stubs for use.
		xtestbed.setup_env(USER_EMAIL='usermail@gmail.com',USER_ID='1', USER_IS_ADMIN='0', overwrite=True)
		xtestbed.activate()
		xtestbed.init_user_stub()
		fake_app = webapp2.WSGIApplication([('/', UserChecker)])

		request = webapp2.Request.blank('/')
		request.method = 'GET'
		response = request.get_response(fake_app)

		ret_val = response.body

		self.assertEqual(response.status_int, 200)
		self.assertEqual(ret_val, 'true')

		xtestbed.deactivate()

	def testVerifyUserLoggedOut(self):
		xtestbed = testbed.Testbed()
		# Then activate the xtestbed, which prepares the service stubs for use.
		xtestbed.setup_env(USER_EMAIL='',USER_ID='', USER_IS_ADMIN='', overwrite=True)
		xtestbed.activate()
		xtestbed.init_user_stub()
		fake_app = webapp2.WSGIApplication([('/', UserChecker)])

		request = webapp2.Request.blank('/')
		request.method = 'GET'
		response = request.get_response(fake_app)

		ret_val = response.body

		self.assertEqual(response.status_int, 200)
		self.assertEqual(ret_val, 'false')

		xtestbed.deactivate()

class TestLoginLogoutHandler(unittest.TestCase):
	def setUp(self):
		pass

	def tearDown(self):
		pass

	def testGetLogoutURL(self):
		xtestbed = testbed.Testbed()
		# Then activate the xtestbed, which prepares the service stubs for use.
		xtestbed.setup_env(USER_EMAIL='usermail@gmail.com',USER_ID='1', USER_IS_ADMIN='0', overwrite=True)
		xtestbed.activate()
		xtestbed.init_user_stub()
		fake_app = webapp2.WSGIApplication([('/userurl/(ins|out)', LoginLogoutURLGetter)])

		request = webapp2.Request.blank('/userurl/out')
		request.method = 'GET'
		response = request.get_response(fake_app)

		self.assertEqual(response.status_int, 200)

		xtestbed.deactivate()

	def testGetLoginURL(self):
		xtestbed = testbed.Testbed()
		# Then activate the xtestbed, which prepares the service stubs for use.
		xtestbed.setup_env(USER_EMAIL='',USER_ID='', USER_IS_ADMIN='', overwrite=True)
		xtestbed.activate()
		xtestbed.init_user_stub()
		fake_app = webapp2.WSGIApplication([('/userurl/(ins|out)', LoginLogoutURLGetter)])

		request = webapp2.Request.blank('/userurl/ins')
		request.method = 'GET'
		response = request.get_response(fake_app)

		self.assertEqual(response.status_int, 200)

		xtestbed.deactivate()