# Note.bk
## CS 498 STK
### Adam Blackburn :: blackbu3
### Kashev Dalmia :: dalmia3
### Matt Vanek :: vanek1

# About
This document is a followable script of tests that a user can follow to test Note.bk. Ideally, this would be automated, but Epic Editor isn't compatible with Selenium IDE or Webdriver due to this bug:

https://code.google.com/p/selenium/issues/detail?id=2331
https://code.google.com/p/selenium/issues/detail?id=2442

Basically, because the Epic Editor uses iframes with content editable divs as the editor, there is no way to automate typing into the editor, which is really the point of Note.bk.

So, without further ado, the test cases:

# Tests

## Test 1 :: Log In
1. Load /
2. Click the Large 'Log In / Sign Up' Button
3. Log In Via Google Users Service
4. At redirect back to /, ensure that the splash screen isn't rendered into the page.
    1. It should now be the Home page.


## Test 2 :: Log Out
1. Ensure logged in (/ displays Home page).
2. Click the button in the top right corner with a power sign.
3. Ensure that after two redirects, ending in /, that the splash screen is rendered, and not the home screen.


## Test 3 :: Make a New Note
1. Ensure logged in (/ displays Home page).
2. Press the '+ Note' button in the top left corner of the screen.
3. Ensure that the Home Screen is hidden and the Note edit screen is displayed.
4. Click the title box and enter some text.
5. Click the editor body and type some text there.
6. Click the 'Done' button in the top left corner of the screen.
7. When the Note edit view appears, ensure the new note is rendered into the note view.


## Test 4 :: Edit An Existing Note
1. Ensure logged in (/ displays Home page).
2. Click on an existing note in the page. (presumably from Test 3)
3. Edit it as in test three.
4. Click 'Done' or escape.
5. Ensure that the note is re-rendered with the updated content in the note view.


## Test 5 :: Delete A Note
1. Ensure logged in (/ displays Home page).
2. Open an existing note by clicking on it.
3. Click 'Delete' in the upper right part of the screen.
4. Ensure that when the note view reappears, that the deleted note is no longer present.


## Test 6 :: Search For A Note
1. Ensure logged in (/ displays Home page).
2. Ensure that there is at least one note in the page.
3. Click the 'Search' button in the top right portion of the screen.
4. Ensure that the search button bar appears.
5. Type a query into the search bar, and either press enter or click the '>' button.
6. Ensure that notes that contain this query remain on the page, and those that don't do not.
7. Ensure that when the 'Search' button is pressed again, that all notes re-appear.

## Test 7 :: Email A Note
1. Ensure logged in (/ displays Home page).
2. Open an existing note.
3. When the note opens, press the envelope button in the upper right hand corner.
4. enter a valid email address in the Box that Appears.
5. Press 'Send'.
6. Ensure that the note arrives at the specified email address.
