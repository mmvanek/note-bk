import unittest
import webapp2

from google.appengine.api import mail
from google.appengine.ext import testbed

from models import NoteModel
from main import MailHandler

class MailTestCaseValidUser(unittest.TestCase):

    def setUp(self):
        self.testbed = testbed.Testbed()
        self.testbed.setup_env(USER_EMAIL='usermail@gmail.com',USER_ID='1', USER_IS_ADMIN='0', overwrite=True)
        self.testbed.activate()
        self.testbed.init_mail_stub()
        self.testbed.init_user_stub()
        self.testbed.init_datastore_v3_stub()
        self.testbed.init_memcache_stub()
        self.mail_stub = self.testbed.get_stub(testbed.MAIL_SERVICE_NAME)

        NoteModel(id='ABCDEFGHIJKLMNOP', contents="note note note", name="note", owner="1", kind=1, order=1).put()

        self.fake_app = webapp2.WSGIApplication([('/note/([A-Za-z0-9]{16})/mail', MailHandler)])

    def tearDown(self):
        self.testbed.deactivate()

    def testMailSent(self):
        request = webapp2.Request.blank('/note/ABCDEFGHIJKLMNOP/mail')
        request.body = 'alice@example.com'
        request.method = 'POST'
        response = request.get_response(self.fake_app)

        self.assertEqual(response.status_int, 200)
        self.assertEqual(response.body, 'ok')

        messages = self.mail_stub.get_sent_messages(to='alice@example.com')
        self.assertEqual(1, len(messages))

    def testMailInvalidEmail(self):
        request = webapp2.Request.blank('/note/ABCDEFGHIJKLMNOP/mail')
        # request.body = ''
        request.method = 'POST'
        response = request.get_response(self.fake_app)

        self.assertEqual(response.status_int, 500)


    def testMailInvalidObject(self):
        request = webapp2.Request.blank('/note/ABCDEFGHIJKLMNO5/mail')
        # request.body = ''
        request.method = 'POST'
        response = request.get_response(self.fake_app)

        self.assertEqual(response.status_int, 404)

class MailTestCaseInvalidUser(unittest.TestCase):

    def setUp(self):
        self.testbed = testbed.Testbed()
        self.testbed.setup_env(USER_EMAIL='',USER_ID='', USER_IS_ADMIN='', overwrite=True)
        self.testbed.activate()
        self.testbed.init_mail_stub()
        self.testbed.init_user_stub()
        self.testbed.init_datastore_v3_stub()
        self.testbed.init_memcache_stub()
        self.mail_stub = self.testbed.get_stub(testbed.MAIL_SERVICE_NAME)

        NoteModel(id='ABCDEFGHIJKLMNOP', contents="note note note", name="note", owner="1", kind=1, order=1).put()

        self.fake_app = webapp2.WSGIApplication([('/note/([A-Za-z0-9]{16})/mail', MailHandler)])

    def tearDown(self):
        self.testbed.deactivate()

    def testMailSent(self):
        request = webapp2.Request.blank('/note/ABCDEFGHIJKLMNOP/mail')
        request.body = 'alice@example.com'
        request.method = 'POST'
        response = request.get_response(self.fake_app)

        self.assertEqual(response.status_int, 401)