import unittest
import webapp2

import main
from models import NoteModel

class RestfulHandlerAlwaysFailTestCase(unittest.TestCase):
	def setUp(self):
		pass

	def tearDown(self):
		pass

	def testSafeToDict(self):
		m = NoteModel(id='ABCDEFGHIJKLMNOP', name="note",
			contents="note note note", owner="1")
		ret_dict = m.safe_to_dict_with_id()

		self.assertTrue('id' in ret_dict)
		self.assertTrue('name' in ret_dict)
		self.assertTrue('contents' in ret_dict)
		self.assertTrue('owner' in ret_dict)

		self.assertEquals(ret_dict['id'], 'ABCDEFGHIJKLMNOP')
		self.assertEquals(ret_dict['name'], 'note')
		self.assertEquals(ret_dict['contents'], 'note note note')
		self.assertEquals(ret_dict['owner'], '1')
