import unittest
import webapp2

import main

from models import NoteModel
from google.appengine.ext import testbed
from google.appengine.api import users

from google.appengine.api import memcache

class TestNumCounter(unittest.TestCase):
	def setUp(self):
		self.testbed = testbed.Testbed()
		self.testbed.setup_env(USER_EMAIL='',USER_ID='', USER_IS_ADMIN='', overwrite=True)
		# Then activate the testbed, which prepares the service stubs for use.
		self.testbed.activate()
		self.testbed.init_user_stub()
		self.testbed.init_datastore_v3_stub()
		self.testbed.init_memcache_stub()
		NoteModel(id='ABCDEFGHIJKLMNOP', contents="note note note", name="note", owner="1", kind=1, order=1).put()
		NoteModel(id='DELETEMEIJKLMNOP', contents="note note note", name="note", owner="1", kind=1, order=1).put()
		self.fake_app = webapp2.WSGIApplication([('/num', main.NoteCounter)])

	def tearDown(self):
		self.testbed.deactivate()

	def testWithMemcacheSet(self):
		memcache.set('note-count', 5)

		request = webapp2.Request.blank('/num')
		request.method = 'GET'
		response = request.get_response(self.fake_app)

		self.assertEqual(response.status_int, 200)
		self.assertEqual(response.body, '5')

	def testWithMemcacheUnset(self):
		memcache.delete('note-count')

		request = webapp2.Request.blank('/num')
		request.method = 'GET'
		response = request.get_response(self.fake_app)

		self.assertEqual(response.status_int, 200)
		self.assertEqual(response.body, '2')
