import sys
import unittest
import coverage

def run_tests(sdk_path, test_path):
	print "Testing..."
	sys.path.insert(0, sdk_path)
	print 'Path', sys.path
	
	import dev_appserver
	dev_appserver.fix_sys_path()

	c = coverage.coverage()

	c.start()

	suite = unittest.loader.TestLoader().discover(test_path)
	unittest.TextTestRunner(verbosity=2).run(suite)

	print 'Tests done.'

	c.stop()

	print 'Stopped coverage...'
	c.report(include=["main.py", "models.py"])

if __name__ == '__main__':
	run_tests('../google_appengine', 'test')
