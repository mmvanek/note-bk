// CS498 STK Final Project
// Note.bk

// Adam Blackburn :: blackbu3
// Kashev Dalmia  :: dalmia3
// Matt Vanek     :: vanek1

// templates.js
// precompiled templates for speed.

// Precompiled using http://dustr.org/

function RegisterPrecompiledTemplates () {
    
    // splash.dust -> splash
    (function() {
        dust.register("splash", body_0);
     
        function body_0(chk, ctx) {
            return chk.write("<div id='header'>Note.bk</div><br><br><div class='center-column'><div class='blurb'><b>Note.bk</b> is a place to store all of your plaintext, Markdown formatted notes. Easily create, modify and search your notes collection from your desktop or mobile device.<br><br><button id='login'class='button'onClick='LogIn()'>Log In / Sign Up</button><div class='blurb bot'>Currently tracking <strong>").reference(ctx.get("num"), ctx, "h").write("</strong> Notes.</div></div></div>");
        }
        return body_0;
    })();
    
    // note.dust -> note
    (function() {
        dust.register("note", body_0);
     
        function body_0(chk, ctx) {
            return chk.write("<div class='note'><div class='note-item-preview'><div class='note-name'>").reference(ctx.get("name"), ctx, "h").write("</div><div class='note-snippet'>").reference(ctx.get("contents"), ctx, "h", ["s"]).write("</div></div></div>");
        }
        return body_0;
    })();
    
    
    // notebk.dust -> notebk
    (function() {
        dust.register("notebk", body_0);
     
        function body_0(chk, ctx) {
            return chk.write("<div id='note-app'><div class='top-buttons'><div id='newnote' class='button'>+ Note</div><div id='searchtoggle' class='button'>Search</div><div id='logout' class='button' onClick='LogOut()'><img src='/static/images/logout.png' height='20'></div><form id='searchbox' action=''><input id='submit' type='submit' value='>'><div><input id='search' type='text' placeholder='Search your Notes'></div></form><hr></div><div class='main'><div id='header'>Note<span class=\"red\">.</span>bk</div><div id='notelist-container-container'><div id='notelist-container'><ul id='notelist'></ul></div></div></div></div><div id='note-editor'><div class='top-buttons'><div id='done' class='button'>Done</div><div id='delete-note' class='button'>Delete</div><div id='email' class='button'>&#9993;</div><input id='title-box' placeholder='New Note Title Here'></div><div class='main'><div id='editorcontainer'><div id='epiceditor'></div></div></div></div><div id='email-box' class='blurb'><div>Enter the email address you'd like to mail this note to.</div><textarea placeholder='e.g. user@domain.com' id='email-entry'></textarea><br><div id='email-buttons'><div id='email-send' class='button'>Send</div><div id='email-cancel' class='button' onClick='emailCancel()'>Cancel</div></div></div>");
        }
        return body_0;
    })();
}