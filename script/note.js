// CS498 STK Final Project
// Note.bk

// Adam Blackburn :: blackbu3
// Kashev Dalmia  :: dalmia3
// Matt Vanek     :: vanek1

// note.js
// Define the Backbone.js Models required to run Note.bk

function LoadNotesApp() {

    // Define our Note Model as a Backbone.js Model.
    var Note = Backbone.Model.extend({
        defaults: function() {
            return {
                name: 'New Note Title',
                contents: 'New Note Content',
                kind: 0,
                order: Date.now(),
            };
        },
    });

    // create a note collection to store all our notes
    var NoteCollection = Backbone.Collection.extend({
        model: Note,
        // use the local storage backend for backbone...
        // TODO we can change this later to support local && server
        url: '/note',

        // // keep notes in sequential order by tracking the order they are added
        // nextOrder: function() {
        //     if (!this.length) return 1;
        //     return this.last().get('order') + 1;
        // },

        searchForString: function(str) {
            this.each(function(e,i) {
                // check to see if string is in the title of the note
                if (e.get('name').toLowerCase().indexOf(str) >= 0) {
                    e.trigger('show');
                // check to see if the string is in the body of the note
                } else if (e.get('contents').toLowerCase().indexOf(str) >= 0) {
                    e.trigger('show');
                // if it's not, hide it from view.
                } else {
                    e.trigger('hide');
                }
            });
            $("#notelist").masonry({
                itemSelector: '.note:visible'
            })
            $("#notelist").masonry('reload');
            console.log('masonry reload called');
        },

        // sort notes by the order
        comparator: function(note){
            return note.get('order');
        }
    });

    // our note collection for this user
    var Notes = new NoteCollection;

    SearchNotes = function(str) {
        Notes.searchForString(str.toLowerCase());
    };

    // create a way to render a note, a Backbone.js View.
    var NoteView = Backbone.View.extend({
        // use a list tag
        tagName:  'li',

        // these events apply to a specific note, not all of them
        events: {
            'click .note' : 'editNote'
        },

        // we have a 1:1 mapping of notes to noteviews, so
        // just map the change and destroy methods to update the view
        initialize: function() {
            this.listenTo(this.model, 'change', this.render);
            this.listenTo(this.model, 'change', this.saveModel);
            this.listenTo(this.model, 'destroy', this.remove);
            this.listenTo(this.model, 'show', this.show);
            this.listenTo(this.model, 'hide', this.hide);
            this.editing = false;
        },


        // Only make REST API calls if the model has truly changed;
        //      Save our Datastore quota.
        saveNote: function() {
            var new_name = $('#title-box').val();
            var new_contents = GetEpicContents();

            if ((this.model.get('name') !== new_name) ||
                (this.model.get('contents') !== new_contents)) {
                this.model.set({
                    name: new_name,
                    contents : new_contents,
                    order: Date.now()
                });
            }

            // this.model.set({
            //     name:$('#title-box').val(),
            //     contents:GetEpicContents()
            // });
        },

        saveModel: function() {
            this.model.save();
        },

        editDone: function() {
            UnsetEpicSave();
            this.saveNote();
            $('.edit-note').show();
            this.editing = false;
            editingNote = undefined;
            Notes.sort();
            console.log('sorting notes');
        },

        editNote: function() {
            $('#title-box').val(this.model.get('name'));
            SetEpicContents(this.model.get('contents'));
            editingNote = this;
            
            $('#note-editor').show();
            $('#note-app').hide();
            this.editing = true;

            SetEpicSave(function(){
                if(editingNote !== undefined) {
                    editingNote.saveNote();
                }
            });
        },

        hide: function() {
            this.$(".note").hide();
        },

        show: function() {
            this.$(".note").show();
        },


        render: function() {
            console.log('rendering note "'+this.model.get('name')+'"');
            var self = this;

            var template_vars = {
                name : this.model.get('name'),
                contents : marked(this.model.get('contents'), {sanitize:true}),
                order : this.model.get('order')
            };

            dust.render("note", template_vars,
                        function(err, out) {
                            self.$el.html(out);
                            self.$("pre code").each(
                                function (i,e) {
                                    hljs.highlightBlock(e);
                                }
                            );
                            self.$("p code").each(
                                function (i,e) {
                                    hljs.highlightBlock(e, null, true);
                                }
                            );
            });

            return this;
        },

        renderDone: function() {
            hljs.highlightBlock(this.$el);
        },

        // if we remove the item, destroy the model
        clear: function() {
            this.model.destroy();
        }

    });

    var editingNote = undefined;

    var AppView = Backbone.View.extend({

        // put it in the #note-app
        el: $('#content'),

        // Delegated events for creating new items, and clearing completed ones.
        events: {
            'click #newnote'      : 'displayNewNote',
            'click #searchtoggle' : 'toggleSearch',
            'click #done'         : 'donePressed',
            'click #cancel'       : 'cancelPressed',
            'click #delete-note'  : 'deleteNote',
            'click #email'        : 'emailPressed'
        },

        initialize: function() {
            this.listenTo(Notes, 'add', this.addOne);
            this.listenTo(Notes, 'reset', this.addAll);
            // this.listenTo(Notes, 'all', this.render);

            this.main = $('#main');

            $("#notelist").masonry({
                itemSelector: '.note',
                columnWidth: 300
            });

            Notes.fetch();
            Notes.sort();
        },

        displayNewNote : function() {
            $('#title-box').val('');
            SetEpicContents('');
            this.$('#note-editor').show();
            this.$('#note-app').hide();
        },

        toggleSearch : function () {
            this.$('#searchbox').toggle(function(){
                if(!($(this).is(':visible'))){
                    // if the search bar isn't visible, show all notes.
                    SearchNotes('');
                }
            });
            this.$('#note-app .main').toggleClass('searchClicked');
        },

        cancelPressed : function () {
            editingNote = undefined;
            // clean up the edit interface, hide it.
            this.$('#note-editor').hide();
            $('#title-box').val('');
            SetEpicContents('');
            
            // reshow the notes viewing page.
            this.$('#note-app').show();
        },

        donePressed : function() {
            if (editingNote) {
                editingNote.editDone();
            } else {
                Notes.create({name:$('#title-box').val(), contents:GetEpicContents()});
            }

            // clean up the edit interface, hide it.
            this.$('#note-editor').hide();
            $('#title-box').val('');
            SetEpicContents('');

            // reshow the notes viewing page.
            this.$('#note-app').show();
            $("#notelist").masonry('reload');
            console.log('masonry reload called');

        },

        emailPressed : function() {
            // var email = prompt('Enter the email address you want to send to',
            //                    'e.g. user@domain.com');
            $('#email-box').show();
            $('#email-send').off('click');
            $('#email-send').on('click', function(){
                sendEmail(editingNote.model.get('id'));
            });
        },

        // render our app
        render: function() {
            Notes.sort();
            console.log('rendering app');
            this.main.show();
            $("#notelist").masonry('reload');
            console.log('masonry reload called');
        },

        // Add a single Note to the list by creating a view for it, and
        // appending its element to the `<ul>`, then re-masonry the list.
        addOne: function(note) {
            var view = new NoteView({model: note});
            //this.$('#notelist').append(view.render().el);
            this.$('#notelist').append(view.render().el);
            $("#notelist").masonry('reload');
            console.log('masonry reload called');
        },

        deleteNote: function() {
            if (editingNote !== undefined) {
                editingNote.model.destroy();
                editingNote = undefined;
            }
            this.$('#note-editor').hide();
            $('#title-box').val('');
            SetEpicContents('');

            this.$('#note-app').show();
            $("#notelist").masonry('reload');
            console.log('masonry reload called');
        }

    });

    var App = new AppView;
}

