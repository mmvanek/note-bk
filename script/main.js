// CS498 STK Final Project
// Note.bk

// Adam Blackburn :: blackbu3
// Kashev Dalmia  :: dalmia3
// Matt Vanek     :: vanek1

// main.js
// waits for the DOM to load, procedes from there.

$(document).ready(function(){
    // Let's get it started in here
    // NotebkStartUncompiled();
    NotebkStart();
});
