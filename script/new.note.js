// CS498 STK Final Project
// Note.bk

// Adam Blackburn :: blackbu3
// Kashev Dalmia  :: dalmia3
// Matt Vanek     :: vanek1

// note.js
// Define the Backbone.js Models required to run Note.bk


/* Note :: Bacbone Model */
var Note = Backbone.Model.extend({
    defaults: function() {
        return {
            'name'     : 'New Note Title',
            'contents' : 'New Note Content',
            'kind'     : 0,
            'modified' : Date.now(),
        };
    },
});

/* NoteView :: Backbone View */
var NoteView = Backbone.View.extend({
    tagName:  'li',

    // these events apply to a specific note, not all of them
    events: {
        'click .note' : 'editNote'
    },

    initialize: function() {
        this.listenTo(this.model, 'change',  this.render);
        this.listenTo(this.model, 'change',  this.saveModel);
        this.listenTo(this.model, 'destroy', this.remove);
        this.listenTo(this.model, 'show',    this.show);
        this.listenTo(this.model, 'hide',    this.hide);
    },


    // Only make REST API calls if the model has truly changed;
    //      Save our Datastore quota.
    saveNote: function() {
        var new_name = $('#title-box').val();
        var new_contents = GetEpicContents();

        if ((this.model.get('name') !== new_name) ||
            (this.model.get('contents') !== new_contents)) {
            this.model.set({
                'name'     : new_name,
                'contents' : new_contents,
                'modified' : Date.now()
            });
        }
    },

    saveModel: function() {
        this.model.save();
    },


    editDone: function() {
        UnsetEpicSave();
        this.saveNote();
        $('.edit-note').show();
    },

    editNote: function() {
        $('#title-box').val(this.model.get('name'));
        SetEpicContents(this.model.get('contents'));
        // swap views.
        $('#note-editor').show();
        $('#note-app').hide();

        SetEpicSave(this.saveNote);
    },

    hide: function() {
        this.$(".note").hide();
    },

    show: function() {
        this.$(".note").show();
    },


    render: function() {
        var self = this;

        var template_vars = {
            'name'     : this.model.get('name'),
            'contents' : marked(this.model.get('contents'), {sanitize:true}),
            'modified' : this.model.get('modified')
        };

        dust.render("note", template_vars,
                    function(err, out) {
                        self.$el.html(out);
                        self.$("pre code").each(
                            function (i,e) {
                                hljs.highlightBlock(e);
                            }
                        );
                        self.$("p code").each(
                            function (i,e) {
                                hljs.highlightBlock(e, null, true);
                            }
                        );
        });

        return this;
    },

    // if we remove the item, destroy the model
    clear: function() {
        this.model.destroy();
    }

});

/* NoteCollection :: Backbone Collection*/
var NoteCollection = Backbone.Collection.extend({
    model: Note,
    
    //localStorage: new Backbone.LocalStorage('notes-backbone'),
    url: '/note',

    initialize : function(){
        this.on('change:modified', this.sort())
    },

    // add a searching function.
    searchForString: function(str) {
        this.each(function(e,i) {
            // check to see if string is in the title of the note
            if (e.get('name').toLowerCase().indexOf(str) >= 0) {
                e.trigger('show');
            // check to see if the string is in the body of the note
            } else if (e.get('contents').toLowerCase().indexOf(str) >= 0) {
                e.trigger('show');
            // if it's not, hide it from view.
            } else {
                e.trigger('hide');
            }
        });
        // reflow the page properly
        $("#notelist").masonry({
            itemSelector: '.note:visible'
        });
        $("#notelist").masonry('reload');
    },

    // sort notes by the order
    comparator: 'order'
});

/* AppView :: Backbone View */
var AppView = Backbone.View.extend({
    el: $('#content'),

    // Delegated events for creating new items, and clearing completed ones.
    events: {
        'click #newnote'      : 'displayNewNote',
        'click #searchtoggle' : 'toggleSearch',
        'click #done'         : 'donePressed',
        'click #cancel'       : 'cancelPressed',
        'click #delete-note'  : 'deleteNote',
        'click .note'         : 'startEdit'
    },

    initialize: function() {
        // the note collection for this AppView.
        this.Notes = new NoteCollection;

        this.listenTo(this.Notes, 'add', this.addOne);
        this.listenTo(this.Notes, 'reset', this.addAll);

        this.main = $('#main');

        $("#notelist").masonry({
            itemSelector: '.note',
            columnWidth: 300
        });

        this.Notes.fetch();
        this.Notes.sort();
    },

    displayNewNote : function() {
        this.Notes.create({}, {

        });
        $('#title-box').val('');
        SetEpicContents('');
        this.$('#note-editor').show();
        this.$('#note-app').hide();
    },

    toggleSearch : function () {
        this.$('#searchbox').toggle(function(){
            if(!($(this).is(':visible'))){
                // if the search bar isn't visible, show all notes.
                SearchNotes('');
            }
        });
        this.$('#note-app .main').toggleClass('searchClicked');
    },

    cancelPressed : function () {
        editingNote = undefined;
        // clean up the edit interface, hide it.
        this.$('#note-editor').hide();
        $('#title-box').val('');
        SetEpicContents('');
        
        // reshow the notes viewing page.
        this.$('#note-app').show();
    },

    donePressed : function() {
        if (editingNote) {
            editingNote.editDone();
        } else {
            this.Notes.create({name:$('#title-box').val(), contents:GetEpicContents()});
        }

        // clean up the edit interface, hide it.
        this.$('#note-editor').hide();
        $('#title-box').val('');
        SetEpicContents('');

        // reshow the notes viewing page.
        this.$('#note-app').show();
        $("#notelist").masonry('reload');
        console.log('masonry reload called');

    },

    // render our app
    render: function() {
        console.log('rendering app');
        this.main.show();
        $("#notelist").masonry('reload');
        console.log('masonry reload called');
    },

    // Add a single Note to the list by creating a view for it, and
    // appending its element to the `<ul>`, then re-masonry the list.
    addOne: function(note) {
        var view = new NoteView({model: note});
        //this.$('#notelist').append(view.render().el);
        this.$('#notelist').append(view.render().el);
        $("#notelist").masonry('reload');
        console.log('masonry reload called');
    },

    deleteNote: function() {
        if (editingNote !== undefined) {
            editingNote.model.destroy();
            editingNote = undefined;
        }
        this.$('#note-editor').hide();
        $('#title-box').val('');
        SetEpicContents('');

        this.$('#note-app').show();
        $("#notelist").masonry('reload');
        console.log('masonry reload called');
    }

});

function LoadNotesApp() {
    // Note collection for this user
    // var Notes = new NoteCollection;

    // App view for this user.
    var App = new AppView;
    var editingNote = undefined;
    // expose searching in the Notes Collection
    SeachNotes = function(str) {
        App.Notes.searchForString(str.toLowerCase());
    };


.
    // used to communicate between NoteView and Appview 
}

