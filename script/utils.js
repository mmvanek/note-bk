// CS498 STK Final Project
// Note.bk

// Adam Blackburn :: blackbu3
// Kashev Dalmia  :: dalmia3
// Matt Vanek     :: vanek1

// utils.js
// utility functions for Note.bk; makes loading templates easy.


// Exists Method : adds an exists method to jQuery. Only for testing.
$.fn.exists = function () {
    return this.length !== 0;
}

// ONLY FOR TESTING (iterating on templates)
// Dust Compiler invoker : given a name, gets /templates/name.dust,
//                         compiles it, and registers it. Then calls
//                         the callback function
// This is only for the uncompiled templates.
function DustCompiler(name, callback){
    $.ajax({
        'type' : 'GET',
        'url': '/templates/'+name+'.dust'
    }).done(function(response){
        var compiled = dust.compile(response, name);
        dust.loadSource(compiled);
        
        if (typeof(callback) == 'function'){ callback(); }
    });
}

// Dust Full Load : given a name and a jQuery selector target,
//                  calls DustCompiler, gets the content, and
//                  injects it into the page via $(target).html()
function DustFullLoad(name, content, target, callback) {
    DustCompiler(name, function(){
        dust.render(name, content, function(err, out){
            if (err != null) {
                return;
            }
            $(target).html(out);

            if (typeof(callback) == 'function'){ callback(); }
        });
    });
}

// DustRender : given a name, content ({}), & target, uses a precompiled
//              Dust template registered using RegisterPrecompiledTemplates()
function DustRender(name, content, target, callback) {
    dust.render(name, content, function(err, out){
        if (err != null) {
        } else {
            $(target).html(out);
            if (typeof(callback) == 'function'){ callback(); }
        }
    });
}

/* EPIC EDITOR FUNCTIONS */
var LoadEpicEditor = function() {
    // options for Epics Editor.
    var options = {
        'container' : 'epiceditor',
        'basePath'  : '/script/lib/epiceditor',
        'theme': {
            'base'    :'/themes/base/epiceditor.css',
            'preview' :'/themes/preview/preview-dark.css',
            'editor'  :'/themes/editor/epic-dark.css'
        },
        'file': {
            'name'           : 'epiceditor',
            'defaultContent' : '',
            'autoSave'       : 200
        }
    };

    // Create the Epic Editor.
    var editor = new EpicEditor(options).load();
    global_editor = editor;

    // Bind All keypresses within Epic editor.
    //      only truly useful for the desktop experience.
    $(editor.getElement('editor').body).keydown(function(e){
        // catch tab key - insert four spaces.
        if (e.keyCode == 9) {
            e.preventDefault();
            editor.getElement('editorIframe').contentWindow.
                document.execCommand(
                    'InsertHTML', false, '&nbsp;&nbsp;&nbsp;&nbsp;'
            );
        // catch escape key - leave edit mode.
        } else if (e.keyCode == 27) {
            $('#done').click();
        }
    });

    // Define Epic handling functions in the global namespace.
    //      These use the local editor defined within the init
    //      function, which is convenient.
    
    // hook up autosave to Backbone.js mechanism.
    SetEpicSave = function(fun) {
        editor.removeListener('save');
        editor.on('save', function(){
            fun();
        })
    }

    UnsetEpicSave = function() {
        editor.removeListener('save');
    }

    GetEpicContents = function () {
        return editor.exportFile();
    }

    SetEpicContents = function (text) {
        editor.importFile('null', text);
    }
}

// IsLoggedIn() - queries the server to find out if
//                the current session is logged in.
//                asynchonous to avoid a string of callbacks.
function IsLoggedIn() {
    var retval = false;
    $.ajax({
        'type'  : 'GET',
        'url'   : '/usercheck',
        'async' : false
    }).done(function(result){
        if(result === 'true') {
            retval = true;
        } else {
            retval = false;
        }
    });

    return retval;
}

// LogInOut() - used by LogIn and LogOut. DRY!
function LogInOut(inout) {
    $.ajax({
        'type' : 'GET',
        'url'  : '/userurl/'+inout
    }).done(function(result){
        window.location = result;
    });
}

// LogIn() - logs the user in from the splash screen by getting a login url.
function LogIn() {
    LogInOut('ins');
}

// LogOut() - logs the user out of any screen. Redirects back to the splash.
function LogOut() {
    LogInOut('out');
}

// emailCancel() - hides the email box
function emailCancel () {
    $('#email-box').hide();
    $('#email-entry').val('');
}

// emailSend() - sends the email
function sendEmail (id) {
    var email = $('#email-entry').val();

    $('#email-box').hide();
    $('#email-entry').val('');

    $.ajax({
        'method' : 'POST',
        'url'    : '/note/'+String(id)+'/mail',
        'data'   : email
    }).done(function(result){
        if (result === 'ok') {
            alert('Message Successfully Sent!');
        } else {
            alert('Message sending failed. Ensure you entered a valid email address.');
        }
    });
}

// NotebkStart() - What is called to set up the app when DOM is ready.
// Two flavors; standard uses precompiled dust templates, one does not.
function NotebkStart () {
    var loggedin = IsLoggedIn();

    // Register the Precompiled Templates.
    RegisterPrecompiledTemplates();

    if (!loggedin)  {
        var numnotes = 'a lot of';
        $.ajax({
            'type' : 'GET',
            'url'  : '/num'
        }).done(function(retval){
            DustRender('splash', {'num' : retval},     '#content');
        }).fail(function(){
            DustRender('splash', {'num' : 'a lot of'}, '#content');
        });
    } else {
        DustRender('notebk', {}, '#content', function(){
            // hide the email box initially.
            $('#email-box').hide();

            LoadEpicEditor(); // Only call this once;
                              // brings into scope the Epic functions.

            // initially hide the note-editor div.
            $('#note-editor').hide();
            // initially hide the search bar
            $('#searchbox').hide();

            // Start the app!
            LoadNotesApp();
            hljs.initHighlightingOnLoad();
            
            // Masonry-ize the unordered list of note previews
            $("#notelist").masonry({
                itemSelector: '.note',
                columnWidth: 300
            });

            // Set form handler on search box.
            $('#searchbox').submit(function(e){
                e.preventDefault();
                SearchNotes($('#search').val());
            });
        });
    }
}

// ONLY FOR TESTING (iterating on templates)
function NotebkStartUncompiled () {
    var loggedin = IsLoggedIn();

    if (!loggedin) {
        var numnotes = 'a lot of';
        $.ajax({
            'type' : 'GET',
            'url'  : '/num'
        }).done(function(retval){
            DustFullLoad('splash', {'num' : retval},     '#content');
        }).fail(function(){
            DustFullLoad('splash', {'num' : 'a lot of'}, '#content');
        });
    } else {
        DustFullLoad('notebk', {}, '#content', function(){
            DustCompiler('note', function() {
                // hide the email box initially.
                $('#email-box').hide();

                LoadEpicEditor(); // Only call this once;
                                  // brings into scope the Epic functions.

                // initially hide the note-editor div.
                $('#note-editor').hide();
                // initially hide the search bar
                $('#searchbox').hide();
                
                // Masonry-ize the unordered list of note previews
                $("#notelist").masonry({
                    'itemSelector' : '.note',
                    'columnWidth'  : 300
                });

                // Start the app!
                LoadNotesApp();
                hljs.initHighlightingOnLoad();
                
                // Set form handler on search box.
                $('#searchbox').submit(function(e){
                    e.preventDefault();
                    SearchNotes($('#search').val());
                });
            });
        });
    }
}
