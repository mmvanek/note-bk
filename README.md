note-bk
=======

# CS498 STK Final Project
## Note.bk

### Adam Blackburn :: blackbu3 :: adam@adamblackburn.net
### Kashev Dalmia  :: dalmia3  :: kashev.dalmia@gmail.com
### Matt Vanek     ::  vanek1  :: matt.vanek@gmail.com


A simple Google app engine note taking app. Works well on mobile and desktop alike. Works offline.

Project Proposal linked [here][prop].
Technical Specification linked [here][tech].
Wireframe documents included in folder non-code/mockups.

## Testing:

### QUnit

QUint tests can be accessed at /quint. Murph has admin access.

### Server tests

Running test_server.py will execute the server tests. There is 100% coverage for the server.

#### Requirements

The server tester requires coverage.py to be installed in the Python PATH and required Google App Engine to be located in the ../google_appengine directory. It must be executed from inside this directory.

## Contents:

### .
Google App Engine Python files.

- main.py - Main application code. Implements REST API for models in models.py, sets URI routing, etc.
- models.py - Defines Datastore models.

### Non-Code
Contains the design mockups, other documents that aren't code.
- url_structure{.vsd,.pdf} - The URL structure of Note.bk.
- mockups
    - note-bk.bmml - The wirefram design file.
    - note-bk_wireframes.pdf - A PDF version of the wireframe designs.

### script
Contains the Javascript files for Note.bk
- main.js - contains the call to initialize the app at page load.
- note.js - defines the Backbone.js models required for operations.
- templates.js - defines a function register precompiled Dust.js templates.
- utils.js - the functions that are frequently used on Note.bk
- lib - the libraries behind Note.bk that aren't easily availible on a CDN.

### static
All the static content of the page, such as .html files.
- images - contains the background image.
- style - the stylesheets for Note.bk
    - main.css - our css.
- index.html - the only static HTML page. All content is rendered into here.
- notebk.appcache - the application manifest.

### templates
Uncompiled Dust.js templates. These aren't used in the production server.


## Libraries Used
### jQuery
If you're not using jQuery, you're probably wrong.

### Dust.js
All of our content is precompiled Dust.js templates injected into a single HTML page.

### Epic Editor
Epic Editor is a Javascript Markdown editor that works well on mobile. It's what is used to edit notes on Note.bk. It has autosaving, live preview, and full screen.

### Backbone.js
A Client Side Model-View library. Allows us to implement a REST API on the server and allow Backbone to take care of the rest, including only sending changes to the model upon change.

### Masonry
A jQuery plugin that does responsive layouts of Notes on Note.bk.


[prop]: https://docs.google.com/document/d/1oD2vm_U0_1IQMkxfP7kY8Mo02cF7PiCgUgr2GdYpqlg/edit?usp=sharing "Note.bk Proposal"
[tech]: https://docs.google.com/document/d/1PYDOa23cnB41yAjD_kBN3LA1wFcYU5CoWp-NyHesbOE/edit?usp=sharing "Note.bk Technical Design Document"
